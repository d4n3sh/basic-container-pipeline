FROM ubuntu:18.04
LABEL maintainer="danesh.manoharan@gmail.com"
RUN apt-get update && \
apt-get upgrade -y && \
apt-get clean

CMD [ "echo", "It's alive!" ]